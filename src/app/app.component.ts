import { Component, OnInit } from '@angular/core';
import { VideoSDKMeeting } from '@videosdk.live/rtc-js-prebuilt';
import { environment } from './../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  async ngOnInit() {

    const config = {
      name: 'Bridge Select user',
      apiKey: '9a10f3ea-26ec-4c98-8dcd-0cfb5f1fdcf6',
      meetingId: 'demo-conference',
      redirectOnLeave: 'https://bridgeselect.com.au/',
      micEnabled: true,
      webcamEnabled: true,
      participantCanToggleSelfWebcam: true,
      participantCanToggleSelfMic: true,

      brandingEnabled: true,
      brandLogoURL: 'https://picsum.photos/200',
      brandName: 'Bridge Select',
      poweredBy: false,

      chatEnabled: true,
      screenShareEnabled: true,
      pollEnabled: true,
      whiteboardEnabled: true,
      raiseHandEnabled: true,

      joinScreen: {
        visible: true, // Show the join screen ?
        title: 'Bridge Select', // Meeting title
        meetingUrl: `${window.location.href}`, // Meeting joining url
      },
    };

    const meeting = new VideoSDKMeeting();
    meeting.init(config);
  }
}
